#include "crypto.hpp"
#include <iostream>

using namespace std;

std::string break_password(std::string target_hex, std::string salt, int iterations, int key_length_in_bits);

int main() {
  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl;

  cout << "The derived key from the PBKDF2 algorithm" << endl;
  std::string key = Crypto::pbkdf2("Password", "Salt");
  cout << Crypto::hex(key) << endl;
  
  std::string target_hex = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  std::string salt = "Saltet til Ola";
  int iterations = 2048;
  int key_length = target_hex.length() * 4;
  cout << "key length in bits: " << key_length << endl;
  
  std::string password = break_password(target_hex, salt, iterations, key_length);
  cout << "passord: " << password << endl;
  
  return 0;
}

std::string break_password(std::string target_hex, std::string salt, int iterations, int key_length_in_bits) {
  std::string password;
  // Checks all 1 char combinations
  cout << "1 char" << endl;
   for (int i = 65; i < 123; i++) {
     password = (char) i;
     if (Crypto::hex(Crypto::pbkdf2(password, salt, iterations, key_length_in_bits)) == target_hex) {
       return password;
     }
   }
   // checks for all 2 char combinations
   cout << "2 chars" << endl;
   for (int i = 65; i < 123; i++) {
     password = (char) i;
     for (int j = 65; j < 123; j++) {
       password.push_back((char)j);
       if (Crypto::hex(Crypto::pbkdf2(password, salt, iterations, key_length_in_bits)) == target_hex) {
         return password;
       }
       else password.pop_back();
     }
   }
   // checks all 3 char combinations
   cout << "3 chars" << endl;
   for (int i = 65; i < 123; i++) {
     password = (char) i;
     for (int j = 65; j < 123; j++) {
       password.push_back((char) j);
       for (int k = 65; k < 123; k++) {
         password.push_back((char)k);
         if (Crypto::hex(Crypto::pbkdf2(password, salt, iterations, key_length_in_bits)) == target_hex) {
           return password;
         }
         else password.pop_back();
       }
       password.pop_back();
       if (i == 121) cout << "searched all strings starting with: " << password << endl;
     }
   }
   return "-1";
}
